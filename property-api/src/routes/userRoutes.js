const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const { authMiddleware, adminMiddleware } = require('../middleware/authMiddleware');

router.post('/register', userController.registerUser);
router.post('/login', userController.loginUser);
router.post('/admin/add', [authMiddleware, adminMiddleware], userController.adminAddUser);
router.post('/admin/approve', [authMiddleware, adminMiddleware], userController.approveUser);
router.post('/admin/decline', [authMiddleware, adminMiddleware], userController.declineUser);
router.put('/update/:userId', authMiddleware, userController.updateUser);
router.put('/admin/update/:userId', [authMiddleware, adminMiddleware], userController.adminUpdateUser);
router.get('/all', [authMiddleware, adminMiddleware], userController.getAllUsers);
router.get('/active', authMiddleware, userController.getActiveUsers);
router.get('/active/company/:companyName', authMiddleware, userController.getActiveUsersByCompanyName);
router.get('/pending', [authMiddleware, adminMiddleware], userController.getAllPendingUsers);
router.get('/me', authMiddleware, userController.getCurrentUser);

module.exports = router;
