const express = require('express');
const router = express.Router();
const itemTaxonController = require('../controllers/itemTaxonController');
const { authMiddleware, adminMiddleware } = require('../middleware/authMiddleware');

router.post('/', [authMiddleware, adminMiddleware], itemTaxonController.createItemTaxon);
router.get('/list', authMiddleware, itemTaxonController.itemTaxonList);
router.get('/all', authMiddleware, itemTaxonController.getAllItemTaxons);
router.get('/:itemTaxonId', authMiddleware, itemTaxonController.getItemTaxonByID);
router.put('/:itemTaxonId', [authMiddleware, adminMiddleware], itemTaxonController.updateItemTaxon);

module.exports = router;
