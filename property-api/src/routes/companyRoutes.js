const express = require('express');
const router = express.Router();
const companyController = require('../controllers/companyController');
const { authMiddleware, adminMiddleware } = require('../middleware/authMiddleware');

router.post('/', [authMiddleware, adminMiddleware], companyController.createCompany);
router.put('/:companyId', [authMiddleware, adminMiddleware], companyController.updateCompany);
router.get('/', authMiddleware, companyController.getAllCompanies);
router.get('/:name', authMiddleware, companyController.getCompanyByName);

module.exports = router;
