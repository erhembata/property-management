const express = require('express');
const router = express.Router();
const itemController = require('../controllers/itemController');
const { authMiddleware, adminMiddleware } = require('../middleware/authMiddleware');

router.post('/', [authMiddleware, adminMiddleware], itemController.createItem);
router.get('/list', authMiddleware, itemController.getItemList);
router.get('/:itemId', authMiddleware, itemController.getItemById);
router.get('/user/:userId', authMiddleware, itemController.getItemsByUser);
router.put('/:itemId', [authMiddleware, adminMiddleware], itemController.updateItem);
router.get('/export/csv', [authMiddleware, adminMiddleware], itemController.exportItemsToCSV);

module.exports = router;
