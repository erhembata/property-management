const bcrypt = require('bcryptjs');

async function encryptPassword(password) {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
}

function comparePassword(candidatePassword, hashedPassword) {
    return bcrypt.compare(candidatePassword, hashedPassword);
}

module.exports = {
    encryptPassword,
    comparePassword
};
