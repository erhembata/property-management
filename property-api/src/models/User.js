const mongoose = require('mongoose');
const { encryptPassword, comparePassword } = require('../utils/passwordUtils');

const userSchema = new mongoose.Schema({
    companyName: {
        type: String,
        ref: 'Company'
    },
    role: {
        type: String,
        enum: ['admin', 'employee', 'none'],
        default: 'none'
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        default: false
    },
    avatar: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        enum: ['pending', 'approved', 'declined'],
        default: 'pending'
    }
}, {
    timestamps: true
});

userSchema.pre('save', async function(next) {
    if (!this.isModified('password')) {
        return next();
    }
    try {
        this.password = await encryptPassword(this.password);
        next();
    } catch (err) {
        next(err);
    }
});

userSchema.methods.comparePassword = function(candidatePassword) {
    return comparePassword(candidatePassword, this.password);
};

const User = mongoose.model('User', userSchema);

module.exports = User;
