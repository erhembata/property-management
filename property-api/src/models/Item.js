const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
    itemTaxonName: {
        type: String,
        ref: 'ItemTaxon',
        required: true
    },
    description: {
        type: String,
        required: true
    },
    identifyingNumber: {
        type: String,
        required: true
    },
    identifyingNumberType: {
        type: String,
        enum: ['serial', 'custom'],
        required: true
    },
    userResponsible: {
        type: String,
        ref: 'User'
    },
    companyName: {
        type: String,
        ref: 'Company'
    }
}, {
    timestamps: true
});

const Item = mongoose.model('Item', itemSchema);

module.exports = Item;

