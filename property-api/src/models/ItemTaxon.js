const mongoose = require('mongoose');

const itemTaxonSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    amtPieces: {
        type: Number,
        default: 0
    },
    description: {
        type: String,
        default: ''
    },
    type: {
        type: String,
        enum: ['office', 'it', 'other'],
        required: true
    }
}, {
    timestamps: true
});

const ItemTaxon = mongoose.model('ItemTaxon', itemTaxonSchema);

module.exports = ItemTaxon;
