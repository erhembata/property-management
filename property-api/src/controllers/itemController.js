const Item = require('../models/Item');
const ItemTaxon = require('../models/ItemTaxon');
const User = require('../models/User');
const Company = require('../models/Company');
const { parseAsync } = require('json2csv');

exports.createItem = async (req, res) => {
    const { itemTaxonName, description, identifyingNumber, identifyingNumberType, userResponsible, companyName } = req.body;
    try {
        const newItem = new Item({
            itemTaxonName,
            description,
            identifyingNumber,
            identifyingNumberType,
            userResponsible,
            companyName
        });
        await newItem.save();

        await ItemTaxon.findOneAndUpdate({ name: itemTaxonName }, { $inc: { amtPieces: 1 } });

        res.status(201).json({ message: 'Item created successfully', item: newItem });
    } catch (error) {
        console.error('Error creating item:', error);
        res.status(500).json({ message: 'Error creating item', error: error.message });
    }
};

exports.getItemList = async (req, res) => {
    const { page = 1, limit = 10, userResponsible, companyName, itemTaxonName } = req.query;
    const filter = {};

    if (userResponsible) filter.userResponsible = userResponsible;
    if (companyName) filter.companyName = companyName;
    if (itemTaxonName) filter.itemTaxonName = itemTaxonName;

    try {
        const items = await Item.find(filter)
            .skip((page - 1) * limit)
            .limit(parseInt(limit));
        const totalItems = await Item.countDocuments(filter);

        res.status(200).json({
            items,
            totalPages: Math.ceil(totalItems / limit),
            currentPage: parseInt(page)
        });
    } catch (error) {
        console.error('Error fetching items:', error);
        res.status(500).json({ message: 'Error fetching items', error: error.message });
    }
};

exports.getItemById = async (req, res) => {
    const { itemId } = req.params;
    try {
        const item = await Item.findById(itemId);
        if (!item) {
            return res.status(404).json({ message: 'Item not found' });
        }
        res.status(200).json(item);
    } catch (error) {
        console.error('Error fetching item:', error);
        res.status(500).json({ message: 'Error fetching item', error: error.message });
    }
};

exports.getItemsByUser = async (req, res) => {
    const { userId } = req.params;
    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }
        const items = await Item.find({ userResponsible: user.firstName });
        res.status(200).json(items);
    } catch (error) {
        console.error('Error fetching items:', error);
        res.status(500).json({ message: 'Error fetching items', error: error.message });
    }
};

exports.updateItem = async (req, res) => {
    const { itemId } = req.params;
    const { itemTaxonName, description, identifyingNumber, identifyingNumberType, userResponsible, companyName } = req.body;
    try {
        const item = await Item.findById(itemId);
        if (!item) {
            return res.status(404).json({ message: 'Item not found' });
        }

        if (itemTaxonName && item.itemTaxonName !== itemTaxonName) {
            await ItemTaxon.findOneAndUpdate({ name: item.itemTaxonName }, { $inc: { amtPieces: -1 } });
            await ItemTaxon.findOneAndUpdate({ name: itemTaxonName }, { $inc: { amtPieces: 1 } });
            item.itemTaxonName = itemTaxonName;
        }

        item.description = description || item.description;
        item.identifyingNumber = identifyingNumber || item.identifyingNumber;
        item.identifyingNumberType = identifyingNumberType || item.identifyingNumberType;
        item.userResponsible = userResponsible || item.userResponsible;
        item.companyName = companyName || item.companyName;

        await item.save();
        res.status(200).json({ message: 'Item updated successfully', item });
    } catch (error) {
        console.error('Error updating item:', error);
        res.status(500).json({ message: 'Error updating item', error: error.message });
    }
};

exports.exportItemsToCSV = async (req, res) => {
    const { userResponsible, companyName, itemTaxonName } = req.query;
    const filter = {};

    if (userResponsible) filter.userResponsible = userResponsible;
    if (companyName) filter.companyName = companyName;
    if (itemTaxonName) filter.itemTaxonName = itemTaxonName;

    try {
        const items = await Item.find(filter);
        const csv = await parseAsync(items, { fields: ['_id', 'itemTaxonName', 'description', 'identifyingNumber', 'identifyingNumberType', 'userResponsible', 'companyName'] });
        res.header('Content-Type', 'text/csv');
        res.attachment('items.csv');
        res.send(csv);
    } catch (error) {
        console.error('Error exporting items to CSV:', error);
        res.status(500).json({ message: 'Error exporting items to CSV', error: error.message });
    }
};
