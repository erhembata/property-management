const Company = require('../models/Company');
const Item = require('../models/Item');

exports.createCompany = async (req, res) => {
    const { name, addresses } = req.body;

    try {
        if (!Array.isArray(addresses) || addresses.length === 0) {
            return res.status(400).json({ message: 'Addresses should be an array and contain at least one address' });
        }

        const newCompany = new Company({
            name,
            addresses
        });

        await newCompany.save();
        res.status(201).json({ message: 'Company created successfully', company: newCompany });
    } catch (error) {
        res.status(500).json({ message: 'Error creating company', error: error.message });
    }
};

exports.updateCompany = async (req, res) => {
    const { companyId } = req.params;
    const { name, newAddress } = req.body;

    try {
        const company = await Company.findById(companyId);
        if (!company) {
            return res.status(404).json({ message: 'Company not found' });
        }

        company.name = name || company.name;

        if (newAddress) {
            company.addresses.unshift(newAddress);
        }

        await company.save();
        res.status(200).json({ message: 'Company updated successfully', company });
    } catch (error) {
        res.status(500).json({ message: 'Error updating company', error: error.message });
    }
};

exports.getAllCompanies = async (req, res) => {
    const { page = 1, limit = 10 } = req.query;

    try {
        const companies = await Company.find()
            .skip((page - 1) * limit)
            .limit(parseInt(limit));

        const companiesWithItemCount = await Promise.all(companies.map(async (company) => {
            try {
                const items = await Item.find({ companyName: company.name });
                const itemCount = items.length;
                const companyObject = company.toObject();
                companyObject.itemCount = itemCount;
                return companyObject;
            } catch (error) {
                const companyObject = company.toObject();
                companyObject.itemCount = 0;
                return companyObject;
            }
        }));

        const totalCompanies = await Company.countDocuments();

        res.status(200).json({
            companies: companiesWithItemCount,
            totalPages: Math.ceil(totalCompanies / limit),
            currentPage: parseInt(page)
        });
    } catch (error) {
        res.status(500).json({ message: 'Error fetching companies', error: error.message });
    }
};

exports.getCompanyByName = async (req, res) => {
    const { name } = req.params;
    try {
        const company = await Company.findOne({ name });
        if (!company) {
            return res.status(404).json({ message: 'Company not found' });
        }
        res.status(200).json(company);
    } catch (error) {
        res.status(500).json({ message: 'Error fetching company', error: error.message });
    }
};
