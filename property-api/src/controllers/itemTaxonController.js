const ItemTaxon = require('../models/ItemTaxon');
const Item = require('../models/Item');

exports.createItemTaxon = async (req, res) => {
    const { name, description, type } = req.body;
    try {
        const newItemTaxon = new ItemTaxon({
            name,
            description: description || '',
            type
        });
        await newItemTaxon.save();
        res.status(201).json({ message: 'Item taxon created successfully', itemTaxon: newItemTaxon });
    } catch (error) {
        console.error('Error creating item taxon:', error);
        res.status(500).json({ message: 'Error creating item taxon', error: error.message });
    }
};

exports.itemTaxonList = async (req, res) => {
    const { page = 1, limit = 10 } = req.query;
    try {
        const itemTaxons = await ItemTaxon.find()
            .skip((page - 1) * limit) 
            .limit(parseInt(limit));
        const totalItemTaxons = await ItemTaxon.countDocuments();

        res.status(200).json({
            itemTaxons,
            totalPages: Math.ceil(totalItemTaxons / limit),
            currentPage: parseInt(page)
        });
    } catch (error) {
        res.status(500).json({ message: 'Error fetching item taxons', error: error.message });
    }
};

exports.getAllItemTaxons = async (req, res) => {
    try {
        const itemTaxons = await ItemTaxon.find();
        res.status(200).json(itemTaxons);
    } catch (error) {
        res.status(500).json({ message: 'Error fetching item taxons', error: error.message });
    }
};

exports.getItemTaxonByID = async (req, res) => {
    const { itemTaxonId } = req.params;
    try {
        const itemTaxon = await ItemTaxon.findById(itemTaxonId);
        if (!itemTaxon) {
            return res.status(404).json({ message: 'Item taxon not found' });
        }
        res.status(200).json(itemTaxon);
    } catch (error) {
        res.status(500).json({ message: 'Error fetching item taxon', error: error.message });
    }
};

exports.updateItemTaxon = async (req, res) => {
    const { itemTaxonId } = req.params;
    const { name, description, type } = req.body;
    try {
        const itemTaxon = await ItemTaxon.findById(itemTaxonId);
        if (!itemTaxon) {
            return res.status(404).json({ message: 'Item taxon not found' });
        }
        itemTaxon.name = name || itemTaxon.name;
        itemTaxon.description = description !== undefined ? description : itemTaxon.description;
        itemTaxon.type = type || itemTaxon.type;

        await itemTaxon.save();
        res.status(200).json({ message: 'Item taxon updated successfully', itemTaxon });
    } catch (error) {
        console.error('Error updating item taxon:', error);
        res.status(500).json({ message: 'Error updating item taxon', error: error.message });
    }
};