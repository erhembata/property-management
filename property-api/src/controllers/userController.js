const jwt = require('jsonwebtoken');
const User = require('../models/User');
const Company = require('../models/Company');
const { encryptPassword, comparePassword } = require('../utils/passwordUtils');

exports.registerUser = async (req, res) => {
    const { firstName, lastName, email, password, phoneNumber } = req.body;

    try {
        const newUser = new User({
            firstName,
            lastName,
            email,
            password,
            phoneNumber,
            avatar: '',
            role: 'none',
            active: false,
            status: 'pending'
        });

        await newUser.save();
        res.status(201).json({ message: 'User registered successfully', user: newUser });
    } catch (error) {
        res.status(500).json({ message: 'Error registering user', error: error.message });
    }
};

exports.approveUser = async (req, res) => {
    const { userId, companyName, role } = req.body;

    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        const company = await Company.findOne({ name: companyName });
        if (!company) {
            return res.status(404).json({ message: 'Company not found' });
        }

        user.role = role;
        user.active = true;
        user.companyName = companyName;
        user.status = 'approved';

        await user.save();
        res.status(200).json({ message: 'User approved successfully', user });
    } catch (error) {
        res.status(500).json({ message: 'Error approving user', error: error.message });
    }
};

exports.declineUser = async (req, res) => {
    const { userId } = req.body;

    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        user.status = 'declined';
        user.active = false;
        user.role = 'none';
        user.companyName = null;

        await user.save();
        res.status(200).json({ message: 'User declined successfully', user });
    } catch (error) {
        res.status(500).json({ message: 'Error declining user', error: error.message });
    }
};

exports.loginUser = async (req, res) => {
    const { credential, password } = req.body;

    try {
        let user;
        if (credential.includes('@')) {
            user = await User.findOne({ email: credential });
        } else {
            user = await User.findOne({ phoneNumber: credential });
        }

        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        if (!user.active || user.role === 'none' || !user.companyName || user.status !== 'approved') {
            return res.status(403).json({ message: 'User not approved or declined' });
        }

        const isMatch = await user.comparePassword(password);
        if (!isMatch) {
            return res.status(400).json({ message: 'Invalid credentials' });
        }

        const token = jwt.sign({ id: user._id, role: user.role }, process.env.JWT_SECRET, { expiresIn: '1h' });

        res.status(200).json({ message: 'Login successful', token, user });
    } catch (error) {
        res.status(500).json({ message: 'Error logging in', error: error.message });
    }
};

exports.updateUser = async (req, res) => {
    const { userId } = req.params;
    const { firstName, lastName, email, phoneNumber, password, avatar } = req.body;

    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        user.firstName = firstName || user.firstName;
        user.lastName = lastName || user.lastName;
        user.email = email || user.email;
        user.phoneNumber = phoneNumber || user.phoneNumber;
        user.avatar = avatar || user.avatar;
        if (password) {
            user.password = await encryptPassword(password);
        }

        await user.save();
        res.status(200).json({ message: 'User updated successfully', user });
    } catch (error) {
        res.status(500).json({ message: 'Error updating user', error: error.message });
    }
};

exports.adminUpdateUser = async (req, res) => {
    const { userId } = req.params;
    const { companyName, role, firstName, lastName, email, phoneNumber, password, active, avatar } = req.body;

    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        const company = await Company.findOne({ name: companyName });
        if (!company) {
            return res.status(404).json({ message: 'Company not found' });
        }

        user.companyName = companyName;
        user.role = role;
        user.firstName = firstName || user.firstName;
        user.lastName = lastName || user.lastName;
        user.email = email || user.email;
        user.phoneNumber = phoneNumber || user.phoneNumber;
        user.avatar = avatar || user.avatar;
        if (password) {
            user.password = await encryptPassword(password);
        }
        user.active = active !== undefined ? active : user.active;

        await user.save();
        res.status(200).json({ message: 'User updated successfully by admin', user });
    } catch (error) {
        res.status(500).json({ message: 'Error updating user by admin', error: error.message });
    }
};

exports.adminAddUser = async (req, res) => {
    const { companyName, role, firstName, lastName, email, password, phoneNumber, avatar } = req.body;

    try {
        const company = await Company.findOne({ name: companyName });
        if (!company) {
            return res.status(404).json({ message: 'Company not found' });
        }

        const encryptedPassword = await encryptPassword(password);

        const newUser = new User({
            companyName,
            role,
            firstName,
            lastName,
            email,
            password: encryptedPassword,
            phoneNumber,
            avatar,
            active: true,
            status: 'approved'
        });

        await newUser.save();
        res.status(201).json({ message: 'User added successfully by admin', user: newUser });
    } catch (error) {
        res.status(500).json({ message: 'Error adding user', error: error.message });
    }
};

exports.getAllUsers = async (req, res) => {
    const { page = 1, limit = 10 } = req.query;

    try {
        const activeUsers = await User.find({ active: true })
            .sort({ role: -1 })
            .skip((page - 1) * limit)
            .limit(Number(limit));
        
        const nonActiveUsers = await User.find({ active: false, status: 'approved' })
            .sort({ role: -1 })
            .skip((page - 1) * limit)
            .limit(Number(limit));

        const declinedUsers = await User.find({ active: false, status: 'declined' })
            .sort({ role: -1 })
            .skip((page - 1) * limit)
            .limit(Number(limit));

        const total = await User.countDocuments();

        res.status(200).json({
            activeUsers,
            nonActiveUsers,
            declinedUsers,
            total
        });
    } catch (error) {
        res.status(500).json({ message: 'Error fetching users', error: error.message });
    }
};

exports.getActiveUsers = async (req, res) => {
    const { page = 1, limit = 10 } = req.query;

    try {
        const activeUsers = await User.find({ active: true })
            .sort({ role: -1 })
            .skip((page - 1) * limit)
            .limit(Number(limit));

        const total = await User.countDocuments({ active: true });

        res.status(200).json({
            activeUsers,
            total
        });
    } catch (error) {
        res.status500json({ message: 'Error fetching active users', error: error.message });
    }
};

exports.getAllPendingUsers = async (req, res) => {
    const { page = 1, limit = 10 } = req.query;

    try {
        const pendingUsers = await User.find({ status: 'pending' })
            .skip((page - 1) * limit)
            .limit(Number(limit));

        const total = await User.countDocuments({ status: 'pending' });

        res.status(200).json({
            pendingUsers,
            total
        });
    } catch (error) {
        res.status(500).json({ message: 'Error fetching pending users', error: error.message });
    }
};

exports.getActiveUsersByCompanyName = async (req, res) => {
    const { companyName } = req.params;
    const { page = 1, limit = 10 } = req.query;

    try {
        const activeUsers = await User.find({ companyName, active: true })
            .sort({ role: -1 })
            .skip((page - 1) * limit)
            .limit(Number(limit));

        const total = await User.countDocuments({ companyName, active: true });

        res.status(200).json({
            activeUsers,
            total
        });
    } catch (error) {
        res.status(500).json({ message: 'Error fetching active users by company name', error: error.message });
    }
};

exports.getCurrentUser = async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        res.json(user);
    } catch (error) {
        res.status(500).json({ message: 'Error fetching user', error: error.message });
    }
};
