// initAdmin.js
const mongoose = require('mongoose');
const User = require('./src/models/User');
const Company = require('./src/models/Company');
const { encryptPassword } = require('./src/utils/passwordUtils');
require('dotenv').config();

const createBases = async () => {
    const db = process.env.MONGO_URI;

    try {
        await mongoose.connect(db, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });

        let company = await Company.findOne({ name: 'Boom' });
        if (!company) {
            company = new Company({ name: 'Boom' });
            await company.save();
            console.log('created');
        } else {
            console.log('already exists');
        }

        let existingAdmin = await User.findOne({ email: 'test@example.com' });
        if (!existingAdmin) {
            const encryptedPassword = await encryptPassword('1234');
            const newUser = new User({
                firstName: 'Base',
                lastName: 'User',
                email: 'test@example.com',
                password: encryptedPassword,
                phoneNumber: '99009900',
                avatar: '',
                role: 'admin',
                active: true,
                status: 'approved',
                companyName: company.name,
            });

            await newUser.save();
            console.log('user created');
        } else {
            console.log('user already exists');
        }

    } catch (error) {
        console.error('Error base entities', error);
    } finally {
        mongoose.connection.close();
    }
};

createBases();
