import axios from 'axios';

export default defineNuxtPlugin((nuxtApp) => {
  const instance = axios.create({
    baseURL: process.env.BACKEND_URL || 'http://localhost:3004', // use your backend URL
  });

  // Add a request interceptor
  instance.interceptors.request.use((config) => {
    // Add the JWT token to headers if available
    const token = localStorage.getItem('token');
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  }, (error) => {
    return Promise.reject(error);
  });

  nuxtApp.provide('axios', instance);
});
