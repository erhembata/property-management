globalThis._importMeta_=globalThis._importMeta_||{url:"file:///_entry.js",env:process.env};import 'node-fetch-native/polyfill';
import { Server as Server$1 } from 'http';
import { Server } from 'https';
import destr from 'destr';
import { eventHandler, setHeaders, sendRedirect, defineEventHandler, handleCacheHeaders, createEvent, getRequestHeader, getRequestHeaders, setResponseHeader, createError, createApp, createRouter as createRouter$1, lazyEventHandler, toNodeListener } from 'h3';
import { createFetch as createFetch$1, Headers } from 'ofetch';
import { createCall, createFetch } from 'unenv/runtime/fetch/index';
import { createHooks } from 'hookable';
import { snakeCase } from 'scule';
import { hash } from 'ohash';
import { parseURL, withQuery, joinURL, withLeadingSlash, withoutTrailingSlash } from 'ufo';
import { createStorage } from 'unstorage';
import defu from 'defu';
import { toRouteMatcher, createRouter } from 'radix3';
import { promises } from 'node:fs';
import { fileURLToPath } from 'node:url';
import { dirname, resolve } from 'pathe';

const _runtimeConfig = {"app":{"baseURL":"/","buildAssetsDir":"/_nuxt/","cdnURL":""},"nitro":{"routeRules":{"/__nuxt_error":{"cache":false}},"envPrefix":"NUXT_"},"public":{}};
const ENV_PREFIX = "NITRO_";
const ENV_PREFIX_ALT = _runtimeConfig.nitro.envPrefix ?? process.env.NITRO_ENV_PREFIX ?? "_";
const getEnv = (key) => {
  const envKey = snakeCase(key).toUpperCase();
  return destr(process.env[ENV_PREFIX + envKey] ?? process.env[ENV_PREFIX_ALT + envKey]);
};
function isObject(input) {
  return typeof input === "object" && !Array.isArray(input);
}
function overrideConfig(obj, parentKey = "") {
  for (const key in obj) {
    const subKey = parentKey ? `${parentKey}_${key}` : key;
    const envValue = getEnv(subKey);
    if (isObject(obj[key])) {
      if (isObject(envValue)) {
        obj[key] = { ...obj[key], ...envValue };
      }
      overrideConfig(obj[key], subKey);
    } else {
      obj[key] = envValue ?? obj[key];
    }
  }
}
overrideConfig(_runtimeConfig);
const config$1 = deepFreeze(_runtimeConfig);
const useRuntimeConfig = () => config$1;
function deepFreeze(object) {
  const propNames = Object.getOwnPropertyNames(object);
  for (const name of propNames) {
    const value = object[name];
    if (value && typeof value === "object") {
      deepFreeze(value);
    }
  }
  return Object.freeze(object);
}

const globalTiming = globalThis.__timing__ || {
  start: () => 0,
  end: () => 0,
  metrics: []
};
const timingMiddleware = eventHandler((event) => {
  const start = globalTiming.start();
  const _end = event.res.end;
  event.res.end = function(chunk, encoding, cb) {
    const metrics = [["Generate", globalTiming.end(start)], ...globalTiming.metrics];
    const serverTiming = metrics.map((m) => `-;dur=${m[1]};desc="${encodeURIComponent(m[0])}"`).join(", ");
    if (!event.res.headersSent) {
      event.res.setHeader("Server-Timing", serverTiming);
    }
    _end.call(event.res, chunk, encoding, cb);
    return this;
  }.bind(event.res);
});

const _assets = {

};

function normalizeKey(key) {
  if (!key) {
    return "";
  }
  return key.split("?")[0].replace(/[/\\]/g, ":").replace(/:+/g, ":").replace(/^:|:$/g, "");
}

const assets$1 = {
  getKeys() {
    return Promise.resolve(Object.keys(_assets))
  },
  hasItem (id) {
    id = normalizeKey(id);
    return Promise.resolve(id in _assets)
  },
  getItem (id) {
    id = normalizeKey(id);
    return Promise.resolve(_assets[id] ? _assets[id].import() : null)
  },
  getMeta (id) {
    id = normalizeKey(id);
    return Promise.resolve(_assets[id] ? _assets[id].meta : {})
  }
};

const storage = createStorage({});

const useStorage = () => storage;

storage.mount('/assets', assets$1);

const config = useRuntimeConfig();
const _routeRulesMatcher = toRouteMatcher(createRouter({ routes: config.nitro.routeRules }));
function createRouteRulesHandler() {
  return eventHandler((event) => {
    const routeRules = getRouteRules(event);
    if (routeRules.headers) {
      setHeaders(event, routeRules.headers);
    }
    if (routeRules.redirect) {
      return sendRedirect(event, routeRules.redirect.to, routeRules.redirect.statusCode);
    }
  });
}
function getRouteRules(event) {
  event.context._nitro = event.context._nitro || {};
  if (!event.context._nitro.routeRules) {
    const path = new URL(event.req.url, "http://localhost").pathname;
    event.context._nitro.routeRules = getRouteRulesForPath(path);
  }
  return event.context._nitro.routeRules;
}
function getRouteRulesForPath(path) {
  return defu({}, ..._routeRulesMatcher.matchAll(path).reverse());
}

const defaultCacheOptions = {
  name: "_",
  base: "/cache",
  swr: true,
  maxAge: 1
};
function defineCachedFunction(fn, opts) {
  opts = { ...defaultCacheOptions, ...opts };
  const pending = {};
  const group = opts.group || "nitro";
  const name = opts.name || fn.name || "_";
  const integrity = hash([opts.integrity, fn, opts]);
  const validate = opts.validate || (() => true);
  async function get(key, resolver) {
    const cacheKey = [opts.base, group, name, key + ".json"].filter(Boolean).join(":").replace(/:\/$/, ":index");
    const entry = await useStorage().getItem(cacheKey) || {};
    const ttl = (opts.maxAge ?? opts.maxAge ?? 0) * 1e3;
    if (ttl) {
      entry.expires = Date.now() + ttl;
    }
    const expired = entry.integrity !== integrity || ttl && Date.now() - (entry.mtime || 0) > ttl || !validate(entry);
    const _resolve = async () => {
      if (!pending[key]) {
        entry.value = void 0;
        entry.integrity = void 0;
        entry.mtime = void 0;
        entry.expires = void 0;
        pending[key] = Promise.resolve(resolver());
      }
      entry.value = await pending[key];
      entry.mtime = Date.now();
      entry.integrity = integrity;
      delete pending[key];
      if (validate(entry)) {
        useStorage().setItem(cacheKey, entry).catch((error) => console.error("[nitro] [cache]", error));
      }
    };
    const _resolvePromise = expired ? _resolve() : Promise.resolve();
    if (opts.swr && entry.value) {
      _resolvePromise.catch(console.error);
      return Promise.resolve(entry);
    }
    return _resolvePromise.then(() => entry);
  }
  return async (...args) => {
    const key = (opts.getKey || getKey)(...args);
    const entry = await get(key, () => fn(...args));
    let value = entry.value;
    if (opts.transform) {
      value = await opts.transform(entry, ...args) || value;
    }
    return value;
  };
}
const cachedFunction = defineCachedFunction;
function getKey(...args) {
  return args.length ? hash(args, {}) : "";
}
function defineCachedEventHandler(handler, opts = defaultCacheOptions) {
  const _opts = {
    ...opts,
    getKey: (event) => {
      const url = event.req.originalUrl || event.req.url;
      const friendlyName = decodeURI(parseURL(url).pathname).replace(/[^a-zA-Z0-9]/g, "").substring(0, 16);
      const urlHash = hash(url);
      return `${friendlyName}.${urlHash}`;
    },
    validate: (entry) => {
      if (entry.value.code >= 400) {
        return false;
      }
      if (entry.value.body === void 0) {
        return false;
      }
      return true;
    },
    group: opts.group || "nitro/handlers",
    integrity: [
      opts.integrity,
      handler
    ]
  };
  const _cachedHandler = cachedFunction(async (incomingEvent) => {
    const reqProxy = cloneWithProxy(incomingEvent.req, { headers: {} });
    const resHeaders = {};
    let _resSendBody;
    const resProxy = cloneWithProxy(incomingEvent.res, {
      statusCode: 200,
      getHeader(name) {
        return resHeaders[name];
      },
      setHeader(name, value) {
        resHeaders[name] = value;
        return this;
      },
      getHeaderNames() {
        return Object.keys(resHeaders);
      },
      hasHeader(name) {
        return name in resHeaders;
      },
      removeHeader(name) {
        delete resHeaders[name];
      },
      getHeaders() {
        return resHeaders;
      },
      end(chunk, arg2, arg3) {
        if (typeof chunk === "string") {
          _resSendBody = chunk;
        }
        if (typeof arg2 === "function") {
          arg2();
        }
        if (typeof arg3 === "function") {
          arg3();
        }
        return this;
      },
      write(chunk, arg2, arg3) {
        if (typeof chunk === "string") {
          _resSendBody = chunk;
        }
        if (typeof arg2 === "function") {
          arg2();
        }
        if (typeof arg3 === "function") {
          arg3();
        }
        return this;
      },
      writeHead(statusCode, headers2) {
        this.statusCode = statusCode;
        if (headers2) {
          for (const header in headers2) {
            this.setHeader(header, headers2[header]);
          }
        }
        return this;
      }
    });
    const event = createEvent(reqProxy, resProxy);
    event.context = incomingEvent.context;
    const body = await handler(event) || _resSendBody;
    const headers = event.res.getHeaders();
    headers.etag = headers.Etag || headers.etag || `W/"${hash(body)}"`;
    headers["last-modified"] = headers["Last-Modified"] || headers["last-modified"] || new Date().toUTCString();
    const cacheControl = [];
    if (opts.swr) {
      if (opts.maxAge) {
        cacheControl.push(`s-maxage=${opts.maxAge}`);
      }
      if (opts.staleMaxAge) {
        cacheControl.push(`stale-while-revalidate=${opts.staleMaxAge}`);
      } else {
        cacheControl.push("stale-while-revalidate");
      }
    } else if (opts.maxAge) {
      cacheControl.push(`max-age=${opts.maxAge}`);
    }
    if (cacheControl.length) {
      headers["cache-control"] = cacheControl.join(", ");
    }
    const cacheEntry = {
      code: event.res.statusCode,
      headers,
      body
    };
    return cacheEntry;
  }, _opts);
  return defineEventHandler(async (event) => {
    if (opts.headersOnly) {
      if (handleCacheHeaders(event, { maxAge: opts.maxAge })) {
        return;
      }
      return handler(event);
    }
    const response = await _cachedHandler(event);
    if (event.res.headersSent || event.res.writableEnded) {
      return response.body;
    }
    if (handleCacheHeaders(event, {
      modifiedTime: new Date(response.headers["last-modified"]),
      etag: response.headers.etag,
      maxAge: opts.maxAge
    })) {
      return;
    }
    event.res.statusCode = response.code;
    for (const name in response.headers) {
      event.res.setHeader(name, response.headers[name]);
    }
    return response.body;
  });
}
function cloneWithProxy(obj, overrides) {
  return new Proxy(obj, {
    get(target, property, receiver) {
      if (property in overrides) {
        return overrides[property];
      }
      return Reflect.get(target, property, receiver);
    },
    set(target, property, value, receiver) {
      if (property in overrides) {
        overrides[property] = value;
        return true;
      }
      return Reflect.set(target, property, value, receiver);
    }
  });
}
const cachedEventHandler = defineCachedEventHandler;

const plugins = [
  
];

function hasReqHeader(event, name, includes) {
  const value = getRequestHeader(event, name);
  return value && typeof value === "string" && value.toLowerCase().includes(includes);
}
function isJsonRequest(event) {
  return hasReqHeader(event, "accept", "application/json") || hasReqHeader(event, "user-agent", "curl/") || hasReqHeader(event, "user-agent", "httpie/") || event.req.url?.endsWith(".json") || event.req.url?.includes("/api/");
}
function normalizeError(error) {
  const cwd = process.cwd();
  const stack = (error.stack || "").split("\n").splice(1).filter((line) => line.includes("at ")).map((line) => {
    const text = line.replace(cwd + "/", "./").replace("webpack:/", "").replace("file://", "").trim();
    return {
      text,
      internal: line.includes("node_modules") && !line.includes(".cache") || line.includes("internal") || line.includes("new Promise")
    };
  });
  const statusCode = error.statusCode || 500;
  const statusMessage = error.statusMessage ?? (statusCode === 404 ? "Not Found" : "");
  const message = error.message || error.toString();
  return {
    stack,
    statusCode,
    statusMessage,
    message
  };
}

const errorHandler = (async function errorhandler(error, event) {
  const { stack, statusCode, statusMessage, message } = normalizeError(error);
  const errorObject = {
    url: event.node.req.url,
    statusCode,
    statusMessage,
    message,
    stack: "",
    data: error.data
  };
  event.node.res.statusCode = errorObject.statusCode !== 200 && errorObject.statusCode || 500;
  if (errorObject.statusMessage) {
    event.node.res.statusMessage = errorObject.statusMessage;
  }
  if (error.unhandled || error.fatal) {
    const tags = [
      "[nuxt]",
      "[request error]",
      error.unhandled && "[unhandled]",
      error.fatal && "[fatal]",
      Number(errorObject.statusCode) !== 200 && `[${errorObject.statusCode}]`
    ].filter(Boolean).join(" ");
    console.error(tags, errorObject.message + "\n" + stack.map((l) => "  " + l.text).join("  \n"));
  }
  if (isJsonRequest(event)) {
    event.node.res.setHeader("Content-Type", "application/json");
    event.node.res.end(JSON.stringify(errorObject));
    return;
  }
  const isErrorPage = event.node.req.url?.startsWith("/__nuxt_error");
  const res = !isErrorPage ? await useNitroApp().localFetch(withQuery(joinURL(useRuntimeConfig().app.baseURL, "/__nuxt_error"), errorObject), {
    headers: getRequestHeaders(event),
    redirect: "manual"
  }).catch(() => null) : null;
  if (!res) {
    const { template } = await import('./error-500.mjs');
    event.node.res.setHeader("Content-Type", "text/html;charset=UTF-8");
    event.node.res.end(template(errorObject));
    return;
  }
  for (const [header, value] of res.headers.entries()) {
    setResponseHeader(event, header, value);
  }
  if (res.status && res.status !== 200) {
    event.node.res.statusCode = res.status;
  }
  if (res.statusText) {
    event.node.res.statusMessage = res.statusText;
  }
  event.node.res.end(await res.text());
});

const assets = {
  "/favicon.ico": {
    "type": "image/vnd.microsoft.icon",
    "etag": "\"26e-Ux1yg+9rPsHUuxtNCWioVmLu5F8\"",
    "mtime": "2024-06-21T05:56:25.115Z",
    "size": 622,
    "path": "../public/favicon.ico"
  },
  "/images/images.png": {
    "type": "image/png",
    "etag": "\"2d51c2-HHRtBzO/e4nFOrd/Qlru7cby5Dg\"",
    "mtime": "2024-06-25T04:26:35.076Z",
    "size": 2970050,
    "path": "../public/images/images.png"
  },
  "/_nuxt/auth.65142f85.js": {
    "type": "application/javascript",
    "etag": "\"64-WxWiqbNZoE4iggmdRjaXQfpY6Eg\"",
    "mtime": "2024-06-25T07:40:42.479Z",
    "size": 100,
    "path": "../public/_nuxt/auth.65142f85.js"
  },
  "/_nuxt/auth.d61aafc4.js": {
    "type": "application/javascript",
    "etag": "\"f6-KMRiBH9orJb4ebtfzTiORbgqNQk\"",
    "mtime": "2024-06-25T07:40:42.479Z",
    "size": 246,
    "path": "../public/_nuxt/auth.d61aafc4.js"
  },
  "/_nuxt/blank.a8b799e0.js": {
    "type": "application/javascript",
    "etag": "\"10a-hLAg2E9hAXElE2b6JHxcF14oyKM\"",
    "mtime": "2024-06-25T07:40:42.479Z",
    "size": 266,
    "path": "../public/_nuxt/blank.a8b799e0.js"
  },
  "/_nuxt/Company.3f5d3e43.js": {
    "type": "application/javascript",
    "etag": "\"162a-MYSLYFxdcj7GYMIOndzOVHhSA58\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 5674,
    "path": "../public/_nuxt/Company.3f5d3e43.js"
  },
  "/_nuxt/composables.eb9ff6a1.js": {
    "type": "application/javascript",
    "etag": "\"61-0blPApsaG5C+GyLK8UAIvuODPcE\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 97,
    "path": "../public/_nuxt/composables.eb9ff6a1.js"
  },
  "/_nuxt/Dashboard.c07b2854.css": {
    "type": "text/css; charset=utf-8",
    "etag": "\"1c5-/LDGAhoQtQEORfMFmNP8K+xubAw\"",
    "mtime": "2024-06-25T07:40:42.480Z",
    "size": 453,
    "path": "../public/_nuxt/Dashboard.c07b2854.css"
  },
  "/_nuxt/Dashboard.f934e05d.js": {
    "type": "application/javascript",
    "etag": "\"4e3-rIMOHEw3d9WmsZ3YmHftSfsbm1U\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 1251,
    "path": "../public/_nuxt/Dashboard.f934e05d.js"
  },
  "/_nuxt/default.56fa1eb0.js": {
    "type": "application/javascript",
    "etag": "\"1c9f-Dvrr32o5XgDaL5sB1WHO9y5vBSA\"",
    "mtime": "2024-06-25T07:40:42.480Z",
    "size": 7327,
    "path": "../public/_nuxt/default.56fa1eb0.js"
  },
  "/_nuxt/Employee.7045422d.js": {
    "type": "application/javascript",
    "etag": "\"351e-pTy0bMEgvMSic15CAT5uiZLR/ns\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 13598,
    "path": "../public/_nuxt/Employee.7045422d.js"
  },
  "/_nuxt/entry.584487d8.css": {
    "type": "text/css; charset=utf-8",
    "etag": "\"a7283-9QdpfJbT8j0tvq+Y6Vw4Z5KtbBg\"",
    "mtime": "2024-06-25T07:40:42.480Z",
    "size": 684675,
    "path": "../public/_nuxt/entry.584487d8.css"
  },
  "/_nuxt/entry.ae38c4c7.js": {
    "type": "application/javascript",
    "etag": "\"2b0894-JiWqoFsIbzXik14FITZJWXs7938\"",
    "mtime": "2024-06-25T07:40:42.480Z",
    "size": 2820244,
    "path": "../public/_nuxt/entry.ae38c4c7.js"
  },
  "/_nuxt/error-component.1d300fed.js": {
    "type": "application/javascript",
    "etag": "\"362-ITOroaGR4x6mrL+rbo8aBii/rhw\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 866,
    "path": "../public/_nuxt/error-component.1d300fed.js"
  },
  "/_nuxt/Icons.baff1a6d.js": {
    "type": "application/javascript",
    "etag": "\"212-8J/DSoPDPA71ZOoQ/a+AUq9Ys8M\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 530,
    "path": "../public/_nuxt/Icons.baff1a6d.js"
  },
  "/_nuxt/index.3c9fcdc2.js": {
    "type": "application/javascript",
    "etag": "\"ec-wEJKj76m0gJUpCxenQwPeVyStFw\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 236,
    "path": "../public/_nuxt/index.3c9fcdc2.js"
  },
  "/_nuxt/Item.352e8606.js": {
    "type": "application/javascript",
    "etag": "\"28c7-SixsgJj8GhX23vViSyzSiSkZhFU\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 10439,
    "path": "../public/_nuxt/Item.352e8606.js"
  },
  "/_nuxt/ItemTaxon.6023a5ce.js": {
    "type": "application/javascript",
    "etag": "\"137b-iOYILHr9wMSCGK70qu3Va/6/frQ\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 4987,
    "path": "../public/_nuxt/ItemTaxon.6023a5ce.js"
  },
  "/_nuxt/Login.e649c68a.js": {
    "type": "application/javascript",
    "etag": "\"7e5-n4OZIZsa2kYLqBYH0cYCPbx5NJ4\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 2021,
    "path": "../public/_nuxt/Login.e649c68a.js"
  },
  "/_nuxt/LoginForm.c077f220.js": {
    "type": "application/javascript",
    "etag": "\"5d9-MXzkT9ZFOayyu8hVGR7FgckqrMc\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 1497,
    "path": "../public/_nuxt/LoginForm.c077f220.js"
  },
  "/_nuxt/materialdesignicons-webfont.5be9e9d7.eot": {
    "type": "application/vnd.ms-fontobject",
    "etag": "\"12aae0-GLTvA08q7BwIed5xQcHFnoNNCXU\"",
    "mtime": "2024-06-25T07:40:42.475Z",
    "size": 1223392,
    "path": "../public/_nuxt/materialdesignicons-webfont.5be9e9d7.eot"
  },
  "/_nuxt/materialdesignicons-webfont.633d596f.woff2": {
    "type": "font/woff2",
    "etag": "\"5d2f8-wtunkFhOlGmtjUyXdeCH4ix7aaA\"",
    "mtime": "2024-06-25T07:40:42.472Z",
    "size": 381688,
    "path": "../public/_nuxt/materialdesignicons-webfont.633d596f.woff2"
  },
  "/_nuxt/materialdesignicons-webfont.7f3afe9b.woff": {
    "type": "font/woff",
    "etag": "\"872e8-V9C6Y3wg5NY7jDb4bLSGK4uK3ak\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 553704,
    "path": "../public/_nuxt/materialdesignicons-webfont.7f3afe9b.woff"
  },
  "/_nuxt/materialdesignicons-webfont.948fce52.ttf": {
    "type": "font/ttf",
    "etag": "\"12aa04-aOk3PGfYI4P3UxgCz4Ny3Zs6JXo\"",
    "mtime": "2024-06-25T07:40:42.476Z",
    "size": 1223172,
    "path": "../public/_nuxt/materialdesignicons-webfont.948fce52.ttf"
  },
  "/_nuxt/Register.83fba908.js": {
    "type": "application/javascript",
    "etag": "\"11a1-UpgmMUFjPwbWconFSr4kCZEcfh8\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 4513,
    "path": "../public/_nuxt/Register.83fba908.js"
  },
  "/_nuxt/Shadow.855c27c3.js": {
    "type": "application/javascript",
    "etag": "\"352-gJKO/GoYvQJpsSZAQZXOwMVn9+U\"",
    "mtime": "2024-06-25T07:40:42.475Z",
    "size": 850,
    "path": "../public/_nuxt/Shadow.855c27c3.js"
  },
  "/_nuxt/Typography.54818705.js": {
    "type": "application/javascript",
    "etag": "\"ec3-rI6GK1PgzCQMS4226FTqC3IuDH8\"",
    "mtime": "2024-06-25T07:40:42.475Z",
    "size": 3779,
    "path": "../public/_nuxt/Typography.54818705.js"
  },
  "/_nuxt/UiParentCard.vue.7217cdd6.js": {
    "type": "application/javascript",
    "etag": "\"212-2kH7aqJToL438i2DTJrVIEU+JRA\"",
    "mtime": "2024-06-25T07:40:42.473Z",
    "size": 530,
    "path": "../public/_nuxt/UiParentCard.vue.7217cdd6.js"
  },
  "/images/background/errorimg.svg": {
    "type": "image/svg+xml",
    "etag": "\"2e72-bTT08T2QvAo6pKTEDF5bCus7r6A\"",
    "mtime": "2024-06-21T05:56:25.117Z",
    "size": 11890,
    "path": "../public/images/background/errorimg.svg"
  },
  "/images/background/rocket.png": {
    "type": "image/png",
    "etag": "\"3c84-AgmtZj5FiuKzhfxY7YNdxAcvX5w\"",
    "mtime": "2024-06-21T05:56:25.118Z",
    "size": 15492,
    "path": "../public/images/background/rocket.png"
  },
  "/images/logos/logo.svg": {
    "type": "image/svg+xml",
    "etag": "\"19e1-6ezBXkOCsCPnA11+NwIkQpPvR5U\"",
    "mtime": "2024-06-21T05:56:25.120Z",
    "size": 6625,
    "path": "../public/images/logos/logo.svg"
  },
  "/images/logos/logolight.svg": {
    "type": "image/svg+xml",
    "etag": "\"19b6-o1C9Igp4McDR0fqD6L/NLpIWgjw\"",
    "mtime": "2024-06-21T05:56:25.122Z",
    "size": 6582,
    "path": "../public/images/logos/logolight.svg"
  },
  "/images/products/s11.jpg": {
    "type": "image/jpeg",
    "etag": "\"388c8-TifUi86aufy7spk20nJYoNGr6c0\"",
    "mtime": "2024-06-21T05:56:25.126Z",
    "size": 231624,
    "path": "../public/images/products/s11.jpg"
  },
  "/images/products/s4.jpg": {
    "type": "image/jpeg",
    "etag": "\"148e6-xG9PQVRtS9qhT0bivYQ9/IEMlfQ\"",
    "mtime": "2024-06-21T05:56:25.130Z",
    "size": 84198,
    "path": "../public/images/products/s4.jpg"
  },
  "/images/products/s5.jpg": {
    "type": "image/jpeg",
    "etag": "\"1e57c-og9Cui0zudpoDzEzIdFMJCkg9CY\"",
    "mtime": "2024-06-21T05:56:25.133Z",
    "size": 124284,
    "path": "../public/images/products/s5.jpg"
  },
  "/images/products/s7.jpg": {
    "type": "image/jpeg",
    "etag": "\"13191-plLCyLTl7UwhKUcQra/8nmxaj5A\"",
    "mtime": "2024-06-21T05:56:25.134Z",
    "size": 78225,
    "path": "../public/images/products/s7.jpg"
  },
  "/images/users/avatar-1.jpg": {
    "type": "image/jpeg",
    "etag": "\"78ea-7LRAJhaoFG6lmZL4jp5DrvfdGYE\"",
    "mtime": "2024-06-21T05:56:25.136Z",
    "size": 30954,
    "path": "../public/images/users/avatar-1.jpg"
  }
};

function readAsset (id) {
  const serverDir = dirname(fileURLToPath(globalThis._importMeta_.url));
  return promises.readFile(resolve(serverDir, assets[id].path))
}

const publicAssetBases = [];

function isPublicAssetURL(id = '') {
  if (assets[id]) {
    return true
  }
  for (const base of publicAssetBases) {
    if (id.startsWith(base)) { return true }
  }
  return false
}

function getAsset (id) {
  return assets[id]
}

const METHODS = ["HEAD", "GET"];
const EncodingMap = { gzip: ".gz", br: ".br" };
const _f4b49z = eventHandler((event) => {
  if (event.req.method && !METHODS.includes(event.req.method)) {
    return;
  }
  let id = decodeURIComponent(withLeadingSlash(withoutTrailingSlash(parseURL(event.req.url).pathname)));
  let asset;
  const encodingHeader = String(event.req.headers["accept-encoding"] || "");
  const encodings = encodingHeader.split(",").map((e) => EncodingMap[e.trim()]).filter(Boolean).sort().concat([""]);
  if (encodings.length > 1) {
    event.res.setHeader("Vary", "Accept-Encoding");
  }
  for (const encoding of encodings) {
    for (const _id of [id + encoding, joinURL(id, "index.html" + encoding)]) {
      const _asset = getAsset(_id);
      if (_asset) {
        asset = _asset;
        id = _id;
        break;
      }
    }
  }
  if (!asset) {
    if (isPublicAssetURL(id)) {
      throw createError({
        statusMessage: "Cannot find static asset " + id,
        statusCode: 404
      });
    }
    return;
  }
  const ifNotMatch = event.req.headers["if-none-match"] === asset.etag;
  if (ifNotMatch) {
    event.res.statusCode = 304;
    event.res.end();
    return;
  }
  const ifModifiedSinceH = event.req.headers["if-modified-since"];
  if (ifModifiedSinceH && asset.mtime) {
    if (new Date(ifModifiedSinceH) >= new Date(asset.mtime)) {
      event.res.statusCode = 304;
      event.res.end();
      return;
    }
  }
  if (asset.type && !event.res.getHeader("Content-Type")) {
    event.res.setHeader("Content-Type", asset.type);
  }
  if (asset.etag && !event.res.getHeader("ETag")) {
    event.res.setHeader("ETag", asset.etag);
  }
  if (asset.mtime && !event.res.getHeader("Last-Modified")) {
    event.res.setHeader("Last-Modified", asset.mtime);
  }
  if (asset.encoding && !event.res.getHeader("Content-Encoding")) {
    event.res.setHeader("Content-Encoding", asset.encoding);
  }
  if (asset.size && !event.res.getHeader("Content-Length")) {
    event.res.setHeader("Content-Length", asset.size);
  }
  return readAsset(id);
});

const _lazy_Aqg5QG = () => import('./renderer.mjs');

const handlers = [
  { route: '', handler: _f4b49z, lazy: false, middleware: true, method: undefined },
  { route: '/__nuxt_error', handler: _lazy_Aqg5QG, lazy: true, middleware: false, method: undefined },
  { route: '/**', handler: _lazy_Aqg5QG, lazy: true, middleware: false, method: undefined }
];

function createNitroApp() {
  const config = useRuntimeConfig();
  const hooks = createHooks();
  const h3App = createApp({
    debug: destr(false),
    onError: errorHandler
  });
  h3App.use(config.app.baseURL, timingMiddleware);
  const router = createRouter$1();
  h3App.use(createRouteRulesHandler());
  for (const h of handlers) {
    let handler = h.lazy ? lazyEventHandler(h.handler) : h.handler;
    if (h.middleware || !h.route) {
      const middlewareBase = (config.app.baseURL + (h.route || "/")).replace(/\/+/g, "/");
      h3App.use(middlewareBase, handler);
    } else {
      const routeRules = getRouteRulesForPath(h.route.replace(/:\w+|\*\*/g, "_"));
      if (routeRules.cache) {
        handler = cachedEventHandler(handler, {
          group: "nitro/routes",
          ...routeRules.cache
        });
      }
      router.use(h.route, handler, h.method);
    }
  }
  h3App.use(config.app.baseURL, router);
  const localCall = createCall(toNodeListener(h3App));
  const localFetch = createFetch(localCall, globalThis.fetch);
  const $fetch = createFetch$1({ fetch: localFetch, Headers, defaults: { baseURL: config.app.baseURL } });
  globalThis.$fetch = $fetch;
  const app = {
    hooks,
    h3App,
    router,
    localCall,
    localFetch
  };
  for (const plugin of plugins) {
    plugin(app);
  }
  return app;
}
const nitroApp = createNitroApp();
const useNitroApp = () => nitroApp;

const cert = process.env.NITRO_SSL_CERT;
const key = process.env.NITRO_SSL_KEY;
const server = cert && key ? new Server({ key, cert }, toNodeListener(nitroApp.h3App)) : new Server$1(toNodeListener(nitroApp.h3App));
const port = destr(process.env.NITRO_PORT || process.env.PORT) || 3e3;
const host = process.env.NITRO_HOST || process.env.HOST;
const s = server.listen(port, host, (err) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
  const protocol = cert && key ? "https" : "http";
  const i = s.address();
  const baseURL = (useRuntimeConfig().app.baseURL || "").replace(/\/$/, "");
  const url = `${protocol}://${i.family === "IPv6" ? `[${i.address}]` : i.address}:${i.port}${baseURL}`;
  console.log(`Listening ${url}`);
});
{
  process.on("unhandledRejection", (err) => console.error("[nitro] [dev] [unhandledRejection] " + err));
  process.on("uncaughtException", (err) => console.error("[nitro] [dev] [uncaughtException] " + err));
}
const nodeServer = {};

export { useRuntimeConfig as a, getRouteRules as g, nodeServer as n, useNitroApp as u };
//# sourceMappingURL=node-server.mjs.map
