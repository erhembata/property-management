const client_manifest = {
  "node_modules/@mdi/font/fonts/materialdesignicons-webfont.eot": {
    "resourceType": "font",
    "mimeType": "font/eot",
    "file": "materialdesignicons-webfont.5be9e9d7.eot",
    "src": "node_modules/@mdi/font/fonts/materialdesignicons-webfont.eot"
  },
  "node_modules/@mdi/font/fonts/materialdesignicons-webfont.woff2": {
    "resourceType": "font",
    "mimeType": "font/woff2",
    "file": "materialdesignicons-webfont.633d596f.woff2",
    "src": "node_modules/@mdi/font/fonts/materialdesignicons-webfont.woff2"
  },
  "node_modules/@mdi/font/fonts/materialdesignicons-webfont.woff": {
    "resourceType": "font",
    "mimeType": "font/woff",
    "file": "materialdesignicons-webfont.7f3afe9b.woff",
    "src": "node_modules/@mdi/font/fonts/materialdesignicons-webfont.woff"
  },
  "node_modules/@mdi/font/fonts/materialdesignicons-webfont.ttf": {
    "resourceType": "font",
    "mimeType": "font/ttf",
    "file": "materialdesignicons-webfont.948fce52.ttf",
    "src": "node_modules/@mdi/font/fonts/materialdesignicons-webfont.ttf"
  },
  "node_modules/nuxt/dist/app/entry.mjs": {
    "resourceType": "script",
    "module": true,
    "file": "entry.ae38c4c7.js",
    "src": "node_modules/nuxt/dist/app/entry.mjs",
    "isEntry": true,
    "dynamicImports": [
      "middleware/auth.js",
      "layouts/auth.vue",
      "layouts/blank.vue",
      "layouts/default.vue",
      "virtual:nuxt:C:/Users/erhem/Documents/Property-Management/property-management/property-front/.nuxt/error-component.mjs"
    ],
    "css": [
      "entry.584487d8.css"
    ],
    "assets": [
      "materialdesignicons-webfont.5be9e9d7.eot",
      "materialdesignicons-webfont.633d596f.woff2",
      "materialdesignicons-webfont.7f3afe9b.woff",
      "materialdesignicons-webfont.948fce52.ttf"
    ]
  },
  "entry.584487d8.css": {
    "file": "entry.584487d8.css",
    "resourceType": "style"
  },
  "materialdesignicons-webfont.5be9e9d7.eot": {
    "file": "materialdesignicons-webfont.5be9e9d7.eot",
    "resourceType": "font",
    "mimeType": "font/eot"
  },
  "materialdesignicons-webfont.633d596f.woff2": {
    "file": "materialdesignicons-webfont.633d596f.woff2",
    "resourceType": "font",
    "mimeType": "font/woff2"
  },
  "materialdesignicons-webfont.7f3afe9b.woff": {
    "file": "materialdesignicons-webfont.7f3afe9b.woff",
    "resourceType": "font",
    "mimeType": "font/woff"
  },
  "materialdesignicons-webfont.948fce52.ttf": {
    "file": "materialdesignicons-webfont.948fce52.ttf",
    "resourceType": "font",
    "mimeType": "font/ttf"
  },
  "virtual:nuxt:C:/Users/erhem/Documents/Property-Management/property-management/property-front/.nuxt/error-component.mjs": {
    "resourceType": "script",
    "module": true,
    "file": "error-component.1d300fed.js",
    "src": "virtual:nuxt:C:/Users/erhem/Documents/Property-Management/property-management/property-front/.nuxt/error-component.mjs",
    "isDynamicEntry": true,
    "imports": [
      "_composables.eb9ff6a1.js",
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "_composables.eb9ff6a1.js": {
    "resourceType": "script",
    "module": true,
    "file": "composables.eb9ff6a1.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "pages/Company.vue": {
    "resourceType": "script",
    "module": true,
    "file": "Company.3f5d3e43.js",
    "src": "pages/Company.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "pages/Dashboard.vue": {
    "resourceType": "script",
    "module": true,
    "file": "Dashboard.f934e05d.js",
    "src": "pages/Dashboard.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ],
    "css": [
      "Dashboard.c07b2854.css"
    ]
  },
  "Dashboard.c07b2854.css": {
    "file": "Dashboard.c07b2854.css",
    "resourceType": "style"
  },
  "pages/Employee.vue": {
    "resourceType": "script",
    "module": true,
    "file": "Employee.7045422d.js",
    "src": "pages/Employee.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "pages/Icons.vue": {
    "resourceType": "script",
    "module": true,
    "file": "Icons.baff1a6d.js",
    "src": "pages/Icons.vue",
    "isDynamicEntry": true,
    "imports": [
      "_UiParentCard.vue.7217cdd6.js",
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "_UiParentCard.vue.7217cdd6.js": {
    "resourceType": "script",
    "module": true,
    "file": "UiParentCard.vue.7217cdd6.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "pages/Item.vue": {
    "resourceType": "script",
    "module": true,
    "file": "Item.352e8606.js",
    "src": "pages/Item.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "pages/ItemTaxon.vue": {
    "resourceType": "script",
    "module": true,
    "file": "ItemTaxon.6023a5ce.js",
    "src": "pages/ItemTaxon.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "pages/auth/Login.vue": {
    "resourceType": "script",
    "module": true,
    "file": "Login.e649c68a.js",
    "src": "pages/auth/Login.vue",
    "isDynamicEntry": true,
    "imports": [
      "_LoginForm.c077f220.js",
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "_LoginForm.c077f220.js": {
    "resourceType": "script",
    "module": true,
    "file": "LoginForm.c077f220.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "pages/auth/Register.vue": {
    "resourceType": "script",
    "module": true,
    "file": "Register.83fba908.js",
    "src": "pages/auth/Register.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "pages/index.vue": {
    "resourceType": "script",
    "module": true,
    "file": "index.3c9fcdc2.js",
    "src": "pages/index.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "pages/ui/Shadow.vue": {
    "resourceType": "script",
    "module": true,
    "file": "Shadow.855c27c3.js",
    "src": "pages/ui/Shadow.vue",
    "isDynamicEntry": true,
    "imports": [
      "_UiParentCard.vue.7217cdd6.js",
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "pages/ui/Typography.vue": {
    "resourceType": "script",
    "module": true,
    "file": "Typography.54818705.js",
    "src": "pages/ui/Typography.vue",
    "isDynamicEntry": true,
    "imports": [
      "_UiParentCard.vue.7217cdd6.js",
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "middleware/auth.js": {
    "resourceType": "script",
    "module": true,
    "file": "auth.65142f85.js",
    "src": "middleware/auth.js",
    "isDynamicEntry": true
  },
  "layouts/auth.vue": {
    "resourceType": "script",
    "module": true,
    "file": "auth.d61aafc4.js",
    "src": "layouts/auth.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "layouts/blank.vue": {
    "resourceType": "script",
    "module": true,
    "file": "blank.a8b799e0.js",
    "src": "layouts/blank.vue",
    "isDynamicEntry": true,
    "imports": [
      "_LoginForm.c077f220.js",
      "node_modules/nuxt/dist/app/entry.mjs"
    ]
  },
  "layouts/default.vue": {
    "resourceType": "script",
    "module": true,
    "file": "default.56fa1eb0.js",
    "src": "layouts/default.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.mjs",
      "_composables.eb9ff6a1.js"
    ]
  },
  "pages/Dashboard.css": {
    "resourceType": "style",
    "file": "Dashboard.c07b2854.css",
    "src": "pages/Dashboard.css"
  },
  "node_modules/nuxt/dist/app/entry.css": {
    "resourceType": "style",
    "file": "entry.584487d8.css",
    "src": "node_modules/nuxt/dist/app/entry.css"
  }
};

export { client_manifest as default };
//# sourceMappingURL=client.manifest.mjs.map
