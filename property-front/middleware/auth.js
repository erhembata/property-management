// middleware/auth.js
export default function ({ store, redirect }) {
    // Хэрэв хэрэглэгч нэвтрээгүй бол
    if (!store.state.auth.loggedIn) {
      return redirect('/login')
    }
  }
  