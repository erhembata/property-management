import {
    LayoutDashboardIcon, LoginIcon, UserIcon, BuildingIcon, LayoutListIcon, ChecklistIcon
} from 'vue-tabler-icons';

export interface menu {
    header?: string;
    title?: string;
    icon?: any;
    to?: string;
    chip?: string;
    chipColor?: string;
    chipVariant?: string;
    chipIcon?: string;
    children?: menu[];
    disabled?: boolean;
    type?: string;
    subCaption?: string;
    action?: string;
}

const sidebarItem: menu[] = [
    { header: 'Нүүр' },
    {
        title: 'Dashboard',
        icon: LayoutDashboardIcon,
        to: '/Dashboard'
    },
    { header: 'Хэрэглэгч' },
    {
        title: 'Компани',
        icon: BuildingIcon,
        to: '/Company'
    },
    {
        title: 'Ажилчид',
        icon: UserIcon,
        to: '/Employee'
    },
    { header: 'Бүртгэл' },
    {
        title: 'Эд зүйлсийн ангилал',
        icon: LayoutListIcon,
        to: '/ItemTaxon'
    },
    {
        title: 'Өмчийн бүртгэл',
        icon: ChecklistIcon,
        to: '/Item'
    },
    { header: 'auth' },
    {
        title: 'Гарах',
        icon: LoginIcon,
        action: 'logout',
        to: '/auth/Login'
    },
];

export default sidebarItem;
