import { ComputedRef, Ref } from 'vue'
export type LayoutKey = "auth" | "blank" | "default"
declare module "C:/Users/erhem/Documents/Property-Management/property-management/property-front/node_modules/nuxt/dist/pages/runtime/composables" {
  interface PageMeta {
    layout?: false | LayoutKey | Ref<LayoutKey> | ComputedRef<LayoutKey>
  }
}