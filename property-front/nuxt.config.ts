// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  runtimeConfig: {
    public: {
      baseurl: "localhost:4004/api"
    }
  },
  ssr: false,
  typescript: {
    shim: false
  },
  build: {
    transpile: ["vuetify"],
  },
  vite: {
    define: {
      "process.env.DEBUG": false,
    },
  },
  nitro: {
    serveStatic: true,
  },
  devServerHandlers: [],
  hooks: {
  },
  plugins: ['~/plugins/axios.js'],
  runtimeConfig: {
    public: {
      base_url: "http://localhost:4004/api"
    },
  },
  router: {
    extendRoutes(routes, resolve) {
      routes.push(
        {
          name: 'admin-dashboard',
          path: '/admin-dashboard',
          component: resolve(__dirname, 'pages/admin/AdminDashboard.vue')
        },
        {
          name: 'user-dashboard',
          path: '/user-dashboard',
          component: resolve(__dirname, 'pages/user/UserDashboard.vue')
        }
      );
    }
  },
})

